﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPClient
{
    public class ChatClient
    {
        private TcpClient client;
        private BinaryReader reader;
        private BinaryWriter writer;
        private Thread receiveThread;
        private bool connected = false;
        public string Username { get; }
        private MainWindow app;
        public bool Accepted { get; }
        public string RejectReason { get; }
        public List<ChatMessage> chatLog;

        public ChatClient(string address, int port, string username, MainWindow window)
        {
            client = new TcpClient(address, port);
            var netStream = client.GetStream();
            reader = new BinaryReader(netStream);
            writer = new BinaryWriter(netStream);
            connected = true;
            Username = username;
            app = window;
            chatLog = new List<ChatMessage>();

            Write(new ChatMessage(MessageType.UserName, DateTime.Now, Username, Username));
            ChatMessage resp;
            while((resp = Read()).Type != MessageType.Response){}
            RejectReason = resp.Text;
            Accepted = resp.Text == "Accept";

            if (Accepted)
            {
                receiveThread = new Thread(Receive);
                receiveThread.Start();
            }
        }

        public ChatMessage Read()
        {
            return ChatMessage.Parse(reader.ReadString());
        }

        public void Write(ChatMessage msg)
        {
            writer.Write(msg.ToString());
        }

        public bool IsConnected()
        {
            return connected && client.Connected;
        }

        private void Receive()
        {
            while(IsConnected())
            {
                try
                {
                    ChatMessage msg = Read();
                    if (msg.Type == MessageType.UserList)
                    {
                        var names = msg.Text.Split('|');
                        app.Invoke(new MethodInvoker(delegate
                        {
                            app.lbUsers.Items.Clear();
                            foreach (string n in names) app.lbUsers.Items.Add(n);
                        }));
                    }
                    else if (msg.Type == MessageType.ChatLog)
                    {
                        var newLog = new List<ChatMessage>();
                        var msgStrings = msg.Text.Split('|');
                        foreach (string s in msgStrings) newLog.Add(ChatMessage.Parse(s));
                        chatLog = newLog;
                        app.Invoke(new MethodInvoker(delegate {
                            app.localLog = chatLog;
                            app.ReloadChat();
                        }));
                    }
                    else
                        app.Invoke(new MethodInvoker(delegate { app.AppendChat(msg); }));
                } catch(Exception e)
                {
                    app.Invoke(new MethodInvoker(delegate {
                        app.AppendChat(new ChatMessage(MessageType.Info, DateTime.Now, "", "Rozłączono z serwerem"));
                        app.ReloadChat();
                        //app.lbLog.Items.Add(e.ToString());
                        app.CloseConnection();
                    }));
                    return;
                    //Disconnect();
                }
            }
            app.Invoke(new MethodInvoker(delegate { app.CloseConnection(); }));
            //Dispose();
        }

        public void Disconnect()
        {
            connected = false;
            try { Dispose(); } catch { }
        }

        public void Dispose()
        {
            if (!client.Connected) return;
            client.GetStream().Close();
            client.Close();
        }
    }
}
