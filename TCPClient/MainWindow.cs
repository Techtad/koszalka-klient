﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPClient
{
    public partial class MainWindow : Form
    {
        private ChatClient chatClient;
        private bool connected = false;
        private bool cancelConnection = false;
        public List<ChatMessage> localLog = new List<ChatMessage>();
        public string localUsername = string.Empty;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            wbChat.Navigate("about:blank");
            wbChat.Document.OpenNew(false);
            wbChat.Document.Write(ChatHTML.BasePage());
            wbChat.Refresh();
            ActiveControl = tbInput;
        }

        private async void bConnect_Click(object sender, EventArgs e)
        {
            bConnect.Enabled = false;
            tbAddress.Enabled = false;
            nudPort.Enabled = false;
            tbUsername.Enabled = false;
            await Task.Run(() =>
            {
                if(!connected)
                {
                    OpenConnection();
                } else
                {
                    CloseConnection();
                }
            });
        }

        private void OpenConnection()
        {
            Invoke(new MethodInvoker(delegate {
                bCancel.Enabled = true;
                bCancel.Visible = true;
            }));

            bool connectionEstablished = false;
            while (!connectionEstablished && !cancelConnection)
            {
                try
                {
                    chatClient = new ChatClient(tbAddress.Text, (int)nudPort.Value, tbUsername.Text, this);
                    connectionEstablished = true;

                    connected = true;
                    Invoke(new MethodInvoker(delegate {
                        bConnect.Text = "Rozłącz";
                        bConnect.Enabled = true;

                        bCancel.Enabled = false;
                        bCancel.Visible = false;

                        tbInput.Enabled = true;
                        bSend.Enabled = true;
                    }));
                }
                catch (Exception e)
                {
                    //Invoke(new MethodInvoker(delegate { lbLog.Items.Add(e.ToString()); }));
                    Thread.Sleep(500);
                }
            }

            if(cancelConnection)
            {
                CloseConnection();
                cancelConnection = false;
                return;
            }

            if (!chatClient.Accepted)
            {
                string reason = chatClient.RejectReason;
                CloseConnection();
                Invoke(new MethodInvoker(delegate { lbLog.Items.Add(DateTime.Now.ToString() + " | Serwer odrzucił nazwę użytkownika"); }));
                MessageBox.Show(reason, "Nazwa użytkownika odrzucona");
            }
            else
            {
                Invoke(new MethodInvoker(delegate
                {
                    localUsername = chatClient.Username;
                    lbLog.Items.Add(DateTime.Now.ToString() + " | Połączono z serwerem");
                }));
            }
        }

        public void CloseConnection()
        {
            connected = false;

            Invoke(new MethodInvoker(delegate {
                if (chatClient != null)
                {
                    chatClient.Disconnect();
                    chatClient.Dispose();
                    chatClient = null;
                    lbLog.Items.Add(DateTime.Now.ToString() + " | Zakończono połączenie");
                }

                lbUsers.Items.Clear();

                bConnect.Text = "Połącz";
                bConnect.Enabled = true;

                tbAddress.Enabled = true;
                nudPort.Enabled = true;
                tbUsername.Enabled = true;
                tbInput.Enabled = false;
                bSend.Enabled = false;
            }));
        }

        private async void tbInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (chatClient != null && e.KeyCode == Keys.Enter && chatClient.IsConnected() && tbInput.Text.Length > 0)
            {
                e.Handled = e.SuppressKeyPress = true;

                await Task.Run(() =>
                {
                    SendMessage();
                });
            }
        }

        private async void bSend_Click(object sender, EventArgs e)
        {
            if (chatClient != null && chatClient.IsConnected() && tbInput.Text.Length > 0)
            {
                await Task.Run(() =>
                {
                    SendMessage();
                });
            }
        }

        private void SendMessage()
        {
            if (tbInput.Text.Length == 0) return;

            ChatMessage msg;
            if (tbInput.Text.StartsWith("/"))
                msg = new ChatMessage(MessageType.Command, DateTime.Now, chatClient.Username, tbInput.Text.Replace("<", "&lt").Substring(1));
            else
                msg = new ChatMessage(MessageType.Text, DateTime.Now, chatClient.Username, tbInput.Text.Replace("<", "&lt"));

            chatClient.Write(msg);
            //AppendChat(msg);

            Invoke(new MethodInvoker(delegate {
                tbInput.Clear();
                tbInput.Select();
                ActiveControl = tbInput;
            }));
        }

        public void AppendChat(ChatMessage msg)
        {
            if (chatClient != null && chatClient.chatLog != null) { chatClient.chatLog.Add(msg); localLog = chatClient.chatLog; }
            else localLog.Add(msg);

            //Invoke(new MethodInvoker(delegate { ReloadChat(); }));
        }

        public void ReloadChat()
        {
            string html = (chatClient != null && chatClient.chatLog != null) ? ChatHTML.GenerateFromLog(chatClient.chatLog, chatClient.Username) : ChatHTML.GenerateFromLog(localLog, localUsername);
            //MessageBox.Show(html);
            wbChat.Document.Write(html);
            wbChat.Refresh();
            wbChat.Document.Body.ScrollIntoView(false);
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private async void bCancel_Click(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                Invoke(new MethodInvoker(delegate {
                    cancelConnection = true;

                    bCancel.Enabled = false;
                    bCancel.Visible = false;

                    lbLog.Items.Add(DateTime.Now.ToString() + " | Anulowano próbę połączenia");
                }));
            });
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wbChat.Document.Write(ChatHTML.BasePage());
            wbChat.Refresh();
            wbChat.Document.Body.ScrollIntoView(false);
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReloadChat();
        }
    }
}
