﻿namespace TCPClient
{
    partial class MainWindow
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "/pm <user>",
            "prywatna wiadomość"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "/mute <user>",
            "wyciszenie użytkownika"}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "/unmute <user>",
            "anulowanie wyciszenia"}, -1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.lAddress = new System.Windows.Forms.Label();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.lPort = new System.Windows.Forms.Label();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.lbLog = new System.Windows.Forms.ListBox();
            this.bConnect = new System.Windows.Forms.Button();
            this.tbInput = new System.Windows.Forms.TextBox();
            this.wbChat = new System.Windows.Forms.WebBrowser();
            this.cmsBrowser = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.lUsername = new System.Windows.Forms.Label();
            this.lbUsers = new System.Windows.Forms.ListBox();
            this.bSend = new System.Windows.Forms.Button();
            this.lUsers = new System.Windows.Forms.Label();
            this.lLog = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.lbCommands = new System.Windows.Forms.Label();
            this.lvCommands = new System.Windows.Forms.ListView();
            this.chCommand = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chExplanation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.cmsBrowser.SuspendLayout();
            this.SuspendLayout();
            // 
            // lAddress
            // 
            this.lAddress.AutoSize = true;
            this.lAddress.Location = new System.Drawing.Point(12, 25);
            this.lAddress.Name = "lAddress";
            this.lAddress.Size = new System.Drawing.Size(34, 13);
            this.lAddress.TabIndex = 0;
            this.lAddress.Text = "Adres";
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(63, 22);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(100, 20);
            this.tbAddress.TabIndex = 1;
            this.tbAddress.Text = "127.0.0.1";
            // 
            // lPort
            // 
            this.lPort.AutoSize = true;
            this.lPort.Location = new System.Drawing.Point(171, 24);
            this.lPort.Name = "lPort";
            this.lPort.Size = new System.Drawing.Size(26, 13);
            this.lPort.TabIndex = 2;
            this.lPort.Text = "Port";
            // 
            // nudPort
            // 
            this.nudPort.Location = new System.Drawing.Point(203, 22);
            this.nudPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(103, 20);
            this.nudPort.TabIndex = 3;
            this.nudPort.Value = new decimal(new int[] {
            1234,
            0,
            0,
            0});
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.HorizontalScrollbar = true;
            this.lbLog.Location = new System.Drawing.Point(614, 73);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(209, 355);
            this.lbLog.TabIndex = 4;
            // 
            // bConnect
            // 
            this.bConnect.Location = new System.Drawing.Point(526, 22);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(75, 20);
            this.bConnect.TabIndex = 5;
            this.bConnect.Text = "Połącz";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // tbInput
            // 
            this.tbInput.CausesValidation = false;
            this.tbInput.Enabled = false;
            this.tbInput.Location = new System.Drawing.Point(12, 434);
            this.tbInput.Name = "tbInput";
            this.tbInput.Size = new System.Drawing.Size(294, 20);
            this.tbInput.TabIndex = 7;
            this.tbInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbInput_KeyDown);
            // 
            // wbChat
            // 
            this.wbChat.AllowNavigation = false;
            this.wbChat.AllowWebBrowserDrop = false;
            this.wbChat.ContextMenuStrip = this.cmsBrowser;
            this.wbChat.IsWebBrowserContextMenuEnabled = false;
            this.wbChat.Location = new System.Drawing.Point(15, 73);
            this.wbChat.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbChat.Name = "wbChat";
            this.wbChat.Size = new System.Drawing.Size(372, 355);
            this.wbChat.TabIndex = 8;
            this.wbChat.WebBrowserShortcutsEnabled = false;
            // 
            // cmsBrowser
            // 
            this.cmsBrowser.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.refreshToolStripMenuItem});
            this.cmsBrowser.Name = "cmsBrowser";
            this.cmsBrowser.Size = new System.Drawing.Size(120, 48);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.clearToolStripMenuItem.Text = "Wyczyść";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.refreshToolStripMenuItem.Text = "Odśwież";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(420, 22);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(100, 20);
            this.tbUsername.TabIndex = 10;
            this.tbUsername.Text = "Użytkownik";
            // 
            // lUsername
            // 
            this.lUsername.AutoSize = true;
            this.lUsername.Location = new System.Drawing.Point(312, 25);
            this.lUsername.Name = "lUsername";
            this.lUsername.Size = new System.Drawing.Size(102, 13);
            this.lUsername.TabIndex = 9;
            this.lUsername.Text = "Nazwa użytkownika";
            // 
            // lbUsers
            // 
            this.lbUsers.FormattingEnabled = true;
            this.lbUsers.HorizontalScrollbar = true;
            this.lbUsers.Location = new System.Drawing.Point(393, 73);
            this.lbUsers.Name = "lbUsers";
            this.lbUsers.Size = new System.Drawing.Size(215, 251);
            this.lbUsers.TabIndex = 11;
            // 
            // bSend
            // 
            this.bSend.Enabled = false;
            this.bSend.Location = new System.Drawing.Point(312, 434);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(75, 20);
            this.bSend.TabIndex = 12;
            this.bSend.Text = "Wyślij";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // lUsers
            // 
            this.lUsers.AutoSize = true;
            this.lUsers.Location = new System.Drawing.Point(449, 57);
            this.lUsers.Name = "lUsers";
            this.lUsers.Size = new System.Drawing.Size(101, 13);
            this.lUsers.TabIndex = 13;
            this.lUsers.Text = "Lista Użytkowników";
            // 
            // lLog
            // 
            this.lLog.AutoSize = true;
            this.lLog.Location = new System.Drawing.Point(676, 57);
            this.lLog.Name = "lLog";
            this.lLog.Size = new System.Drawing.Size(90, 13);
            this.lLog.TabIndex = 14;
            this.lLog.Text = "Dziennik Zdarzeń";
            // 
            // bCancel
            // 
            this.bCancel.Enabled = false;
            this.bCancel.Location = new System.Drawing.Point(607, 22);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 20);
            this.bCancel.TabIndex = 15;
            this.bCancel.Text = "Anuluj";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Visible = false;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // lbCommands
            // 
            this.lbCommands.AutoSize = true;
            this.lbCommands.Location = new System.Drawing.Point(468, 330);
            this.lbCommands.Name = "lbCommands";
            this.lbCommands.Size = new System.Drawing.Size(71, 13);
            this.lbCommands.TabIndex = 16;
            this.lbCommands.Text = "Lista Komend";
            // 
            // lvCommands
            // 
            this.lvCommands.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chCommand,
            this.chExplanation});
            this.lvCommands.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3});
            this.lvCommands.Location = new System.Drawing.Point(393, 346);
            this.lvCommands.Name = "lvCommands";
            this.lvCommands.Size = new System.Drawing.Size(215, 82);
            this.lvCommands.TabIndex = 17;
            this.lvCommands.UseCompatibleStateImageBehavior = false;
            this.lvCommands.View = System.Windows.Forms.View.Details;
            // 
            // chCommand
            // 
            this.chCommand.Text = "Polecenie";
            this.chCommand.Width = 85;
            // 
            // chExplanation
            // 
            this.chExplanation.Text = "Działanie";
            this.chExplanation.Width = 125;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 479);
            this.Controls.Add(this.lvCommands);
            this.Controls.Add(this.lbCommands);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.lLog);
            this.Controls.Add(this.lUsers);
            this.Controls.Add(this.bSend);
            this.Controls.Add(this.lbUsers);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.lUsername);
            this.Controls.Add(this.wbChat);
            this.Controls.Add(this.tbInput);
            this.Controls.Add(this.bConnect);
            this.Controls.Add(this.lbLog);
            this.Controls.Add(this.nudPort);
            this.Controls.Add(this.lPort);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.lAddress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Klient Czatu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.cmsBrowser.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lAddress;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Label lPort;
        private System.Windows.Forms.NumericUpDown nudPort;
        public System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.TextBox tbInput;
        public System.Windows.Forms.WebBrowser wbChat;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label lUsername;
        public System.Windows.Forms.ListBox lbUsers;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.Label lUsers;
        private System.Windows.Forms.Label lLog;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.ContextMenuStrip cmsBrowser;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.Label lbCommands;
        private System.Windows.Forms.ListView lvCommands;
        private System.Windows.Forms.ColumnHeader chCommand;
        private System.Windows.Forms.ColumnHeader chExplanation;
    }
}

